#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_solve, diplomacy_read, diplomacy_print, diplomacy_run

class MyUnitTest (TestCase):
    # ---
    # diplomacy_solve
    # ---
    def test_diplomacy_solve_1(self):
        I = ["A Sydney Hold","B Chicago Move Houston","C Beijing Move Houston","D London Support B","E Austin Support C"]
        R = diplomacy_solve(I)
        self.assertEqual(R,["A Sydney","B [dead]","C [dead]","D London","E Austin"])

    def test_diplomacy_solve_2(self):
        I = ["A Toronto Move Seattle","G Tokoyo Move Seattle","L Lima Move Seattle","R Auckland Support G","W Paris Move Auckland"]
        R = diplomacy_solve(I)
        self.assertEqual(R,["A [dead]","G [dead]","L [dead]","R [dead]","W [dead]"])

    def test_diplomacy_solve_3(self):
        I = ["E Singapore Hold","J Hanoi Move Singapore","Z Cairo Hold"]
        R = diplomacy_solve(I)
        self.assertEqual(R,["E [dead]","J [dead]","Z Cairo"])

    # ---
    # diplomacy_read
    # ---

    def test_diplomacy_read_1(self):
        s = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        v = diplomacy_read(s)
        self.assertEqual(v,['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B'])

    def test_diplomacy_read_2(self):
        s = StringIO("A Sydney Hold\nB Chicago Move Houston\nC Beijing Move Houston\nD London Support B\nE Austin Support C\n")
        v = diplomacy_read(s)
        self.assertEqual(v,["A Sydney Hold","B Chicago Move Houston","C Beijing Move Houston","D London Support B","E Austin Support C"])

    def test_diplomacy_read_3(self):
        s = StringIO("A Toronto Move Seattle\nG Tokoyo Move Seattle\nL Lima Move Seattle\nR Auckland Support G\nW Paris Move Auckland")
        v = diplomacy_read(s)
        self.assertEqual(v,["A Toronto Move Seattle","G Tokoyo Move Seattle","L Lima Move Seattle","R Auckland Support G","W Paris Move Auckland"])

    # ---
    # diplomacy_print
    # ---

    def test_diplomacy_print1(self):
        A = Army("A", "Sydney", move_city = None)
        B = Army("B", "Toronto", move_city = None)
        C = Army("C", "London", move_city = None)

        w = StringIO()
        diplomacy_print(w, [A,B,C])
        self.assertEqual(w.getvalue(), "A Sydney\nB Toronto\nC London\n")

    def test_diplomacy_print2(self):
        A = Army("A", "Sydney", move_city = None)
        B = Army("B", "Toronto", move_city = None)
        C = Army("C", "London", move_city = None)

        w = StringIO()
        diplomacy_print(w, [A,B,C])
        self.assertEqual(w.getvalue(), "A Sydney\nB Toronto\nC London\n")

    def test_diplomacy_print3(self):
        A = Army("A", "Sydney", move_city = None)
        B = Army("B", "Toronto", move_city = "RioDeJanerio")
        C = Army("C", "London", move_city = None)
        B.status, C.status = 'dead', 'dead'

        w = StringIO()
        diplomacy_print(w, [A,B,C])
        self.assertEqual(w.getvalue(), "A Sydney\nB [dead]\nC [dead]\n")

    # ---
    # diplomacy_run
    # ---

    def test_diplomacy_run1(self):
        I = StringIO("A Atlanta Hold\nB Berlin Move Atlanta\nC Cario Hold\n")
        W = StringIO()
        diplomacy_run(I,W)
        self.assertEqual(W.getvalue(), "A [dead]\nB [dead]\nC Cario\n")

    def test_diplomacy_run2(self):
        I = StringIO("J Jerusalem Move Kyoto\nK Kyoto Move Leon\nL Leon Move Jerusalem\n")
        W = StringIO()
        diplomacy_run(I,W)
        self.assertEqual(W.getvalue(), "J Kyoto\nK Leon\nL Jerusalem\n")

    def test_diplomacy_run3(self):
        I = StringIO("G SanDiego Hold\nN Berlin Move SanDiego\nS Perth Support G\nV NewYork Support G Hold\nZ Dublin Support N\n")
        W = StringIO()
        diplomacy_run(I,W)
        self.assertEqual(W.getvalue(), "G [dead]\nN [dead]\nS Perth\nV NewYork\nZ Dublin\n")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()
