import Diplomacy
from io import StringIO
import unittest

class DiplomacyUnitTests(unittest.TestCase):
    def test_read_1(self):
        test = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Barcelona")
        ret = Diplomacy.diplomacy_read(test.getvalue())
        expected = {
                "Move":[
                    {"D":"Barcelona"}
                    ],
                "Hold":[
                    {"A":"Madrid"},
                    ],
                "Support":[
                    {"B":["Barcelona","A"]},
                    {"C":["London","B"]}
                    ]
                }
        self.assertEqual(ret, expected)

    def test_eval_1(self):
        # fixed
        test_scenario = {
            "Move":[
                {"B":"Barcelona"},
            ],
            "Hold":[
                {"A":"Madrid"}
            ],
            "Support":[
                {"C":["London","B"]}
            ]
        }
        ret = Diplomacy.diplomacy_eval(test_scenario)

        expected_ret = {
            "A":"Madrid",
            "B":"Barcelona",
            "C":"London",
        }

        self.assertEqual(ret,expected_ret)

    def test_eval_2(self):
        # fixed
        test_scenario = {
            "Move":[
                {"B":"Madrid"},
                {"C":"Madrid"}
            ],
            "Hold":[
                {"A":"Madrid"}
            ],
            "Support":[
            ]
        }
        ret = Diplomacy.diplomacy_eval(test_scenario)

        expected_ret = {
            "A":"[dead]",
            "B":"[dead]",
            "C":"[dead]",
        }

        self.assertEqual(ret,expected_ret)

    def test_solve_1(self):
        test = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Barcelona")
        ret = StringIO()
        Diplomacy.diplomacy_solve(test,ret)
        self.assertEqual(ret.getvalue(),"A Madrid\nB Barcelona\nC London\nD [dead]\n")

    def test_solve_2(self):
        test = StringIO("A Madrid Hold\nB Barcelona Support A\nC Austin Support B\nD London Move Barcelona\nE Dallas Move Madrid")
        ret = StringIO()
        Diplomacy.diplomacy_solve(test,ret)
        self.assertEqual(ret.getvalue(),"A [dead]\nB Barcelona\nC Austin\nD [dead]\nE [dead]\n")

    def test_solve_3(self):
        test = StringIO("A Madrid Hold\nB Austin Move Madrid\nC London Move Austin")
        ret = StringIO()
        Diplomacy.diplomacy_solve(test,ret)
        self.assertEqual(ret.getvalue(),"A [dead]\nB [dead]\nC Austin\n")

    def test_solve_4(self):
        test = StringIO("A Austin Hold\nB Dallas Support A\nC Madrid Move Austin\nD Norvinsk Support C\nE Vancouver Move Calgary")
        ret = StringIO()
        Diplomacy.diplomacy_solve(test,ret)
        self.assertEqual(ret.getvalue(),"A [dead]\nB Dallas\nC [dead]\nD Norvinsk\nE Calgary\n")

    def test_solve_5(self):
        test = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Support E\nE Austin Move Madrid")
        ret = StringIO()
        Diplomacy.diplomacy_solve(test,ret)
        self.assertEqual(ret.getvalue(),"A [dead]\nB Barcelona\nC London\nD Paris\nE [dead]\n")

if __name__ == "__main__":
    unittest.main()
