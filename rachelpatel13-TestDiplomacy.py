#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Rachel Patel
# Daniel Kim
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Austin Hold\nB Madrid Move Austin\nC London Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Austin', 'Hold'], ['B', 'Madrid', 'Move', 'Austin'], ['C', 'London', 'Support', 'B']])

    def test_read_2(self):
        s = "A Dallas Move Chicago\nB Chicago Hold\nC Houston Hold\nD Pittsburg Support C\nE Boston Move Chicago\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Dallas', 'Move', 'Chicago'], ['B', 'Chicago', 'Hold'], ['C', 'Houston', 'Hold'], ['D', 'Pittsburg', 'Support', 'C'], ['E', 'Boston', 'Move', 'Chicago']])

    def test_read_3(self): 
        s = "A Seoul Move Madrid\nB Barcelona Move Seoul\nC Madrid Support B\nD London Move Seoul\nF Dallas Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Seoul', 'Move', 'Madrid'], ['B', 'Barcelona', 'Move', 'Seoul'], ['C', 'Madrid', 'Support', 'B'], ['D', 'London', 'Move', 'Seoul'], ['F', 'Dallas', 'Support', 'B']])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Austin', 'Hold'], ['B', 'Madrid', 'Move', 'Austin'], ['C', 'London', 'Support', 'B']])
        self.assertEqual(v, ['A', '[dead]', 'B', 'Austin', 'C', 'London'])

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Dallas', 'Move', 'Chicago'], ['B', 'Chicago', 'Hold'], ['C', 'Houston', 'Hold'], ['D', 'Pittsburg', 'Support', 'C'], ['E', 'Boston', 'Move', 'Chicago']])
        self.assertEqual(v, ['A', '[dead]', 'B', '[dead]', 'C', 'Houston', 'D', 'Pittsburg', 'E', '[dead]'])

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, ['A', '[dead]', 'B', '[dead]', 'C', 'Houston', 'D', 'Pittsburg', 'E', '[dead]'])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Houston\nD Pittsburg\nE [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ['A', 'Dallas', 'B', '[dead]', 'C', '[dead]'])
        self.assertEqual(w.getvalue(), "A Dallas\nB [dead]\nC [dead]\n")

    def test_print_3(self): 
        w = StringIO()
        diplomacy_print(w, ['A', '[dead]', 'B', 'Austin', 'C', 'London'])
        self.assertEqual(w.getvalue(), "A [dead]\nB Austin\nC London\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Austin Hold\nB Madrid Move Austin\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC London\n")
    
    def test_solve_2(self): 
        r = StringIO("A Dallas Move Chicago\nB Chicago Hold\nC Houston Hold\nD Pittsburg Support C\nE Boston Move Chicago\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Houston\nD Pittsburg\nE [dead]\n")

    '''
    def test_solve_3(self): 
        r = StringIO("45, 90\n")
        w  = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "45 90 116\n")
    '''

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
